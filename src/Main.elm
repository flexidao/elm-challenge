module Main exposing (main)

import Browser exposing (UrlRequest)
import Browser.Navigation exposing (Key)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as D exposing (Decoder)
import Json.Decode.Pipeline as DP
import RemoteData exposing (RemoteData(..), WebData)
import Time exposing (Posix, millisToPosix)
import Url
import Url.Parser exposing ((<?>))
import Url.Parser.Query


type Msg
    = SetQuery String
    | URLRequest UrlRequest
    | UrlChange Url.Url


type alias Flags =
    {}


main : Program Flags Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , onUrlRequest = URLRequest
        , onUrlChange = UrlChange
        }



-- HELPERS


parseQuery : Url.Url -> Maybe String
parseQuery url =
    url
        |> Debug.log "url"
        |> Url.Parser.parse (Url.Parser.top <?> Url.Parser.Query.string "q")
        |> Maybe.andThen identity



-- CONSTANTS


debounceDelay : Posix
debounceDelay =
    millisToPosix 100



-- MODEL


type alias Heroes =
    {}


type alias Model =
    { query : String
    , heroes : WebData Heroes
    , key : Key
    }


init : Flags -> Url.Url -> Key -> ( Model, Cmd Msg )
init flags location key =
    let
        query =
            Maybe.withDefault "" (parseQuery location)
    in
    ( { query = query
      , heroes = Loading
      , key = key
      }
    , getheroes query
    )



-- UPDATE


nextUrl : String -> String
nextUrl query =
    if String.length query == 0 then
        "/"

    else
        "/?q=" ++ Url.percentEncode query


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetQuery query ->
            ( { model
                | query = query
              }
            , Cmd.none
            )

        URLRequest urlRequest ->
            ( model, Cmd.none )

        UrlChange url ->
            ( model, Cmd.none )



-- VIEW


view : Model -> Browser.Document Msg
view { query, heroes } =
    { title = ""
    , body =
        [ div [ class "app" ]
            [ viewSearchInput query
            , viewheroesWebData heroes
            ]
        ]
    }


viewSearchInput : String -> Html Msg
viewSearchInput query =
    input
        [ type_ "search"
        , placeholder "Search heroes..."
        , class "search-input"
        , value query
        , onInput SetQuery
        ]
        [ text query ]


viewheroesWebData : WebData Heroes -> Html Msg
viewheroesWebData heroesWebData =
    case heroesWebData of
        NotAsked ->
            text "Not Asked."

        Loading ->
            text "Loading."

        Failure error ->
            text "error"

        Success heroes ->
            text "success"



-- HTTP


heroesUrl : String -> String
heroesUrl query =
    "http://localhost:8000/api/heroes?q=" ++ query


getheroes : String -> Cmd Msg
getheroes query =
    Cmd.none
