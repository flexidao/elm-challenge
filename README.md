# Elm Challenge

## Setup
- `yarn`
- `elm-package install`

## Running
- `yarn run api`
- `yarn run dev`
- open `http://localhost:3000`

## Demo

![alt tag](heroes.gif)
